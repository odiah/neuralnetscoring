package com.sysomos.neuralnet.scoring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.beust.jcommander.JCommander;
import com.sysomos.neuralnet.scoring.Scorecard.Parameters;

public class ScorecardTest {

	private Parameters params;
	private Scorecard scorecard;
	private static List<String> posts = new ArrayList<String>();
	private static final List<String> ARG_QUERIES = new ArrayList<String>();

	static {
		posts.add("Winter is coming");
		posts.add("I like winter but hate snow");
		posts.add("I like snow but hate cold");
		posts.add("LOL, LOL, boots, shoes, shoe");

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-m",
				"/Users/odia/Documents/workspace/neuralnet-scoring/model-conf.dat:/Users/odia/Documents/workspace/neuralnet-scoring/model-weights.dat",
				"-v", "/Users/odia/Documents/workspace/neuralnet-scoring/words.txt" }));
	}

	private static final String ARGS[] = ARG_QUERIES
			.toArray(new String[ARG_QUERIES.size()]);

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		params = new Parameters();
		new JCommander(params, ARGS).parse();
		scorecard = new Scorecard(params);
	}

	@Test
	public void fitTest() {
		System.out.println("Predicted class: " + scorecard.fit(posts));
	}
}
