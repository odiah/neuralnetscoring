package com.sysomos.neuralnet.scoring.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.nd4j.linalg.api.ndarray.INDArray;

import com.sysomos.text.sentenceiterator.CollectionSentenceIterator;
import com.sysomos.text.tokenizer.factory.DefaultTokenizerFactory;
import com.sysomos.text.utils.StopWords;
import com.sysomos.word2vec.VocabWord;
import com.sysomos.word2vec.word.vectors.WordVectors;
import com.sysomos.word2vec.words.vectorizer.TextVectorizer;
import com.sysomos.word2vec.words.vectorizer.TfidfVectorizer;
import com.sysomos.word2vec.wordstore.VocabCache;

public class ScoringUtils {
	private static final int MIN_WORD_FREQ = 1;

	/**
	 * Return the vocabulary where the minimum word frequency is 1.
	 * 
	 * @param data
	 *            Collection of data
	 * @return Vocabulary cache @see {@link VocabCache}
	 */
	public static VocabCache getVocab(Collection<String> data) {
		return getVocab(data, MIN_WORD_FREQ);
	}

	public static VocabCache getVocab(Collection<String> data,
			int minWordFreq) {
		if (CollectionUtils.isEmpty(data))
			return null;
		TextVectorizer vectorizer = new TfidfVectorizer.Builder()
				.iterate(new CollectionSentenceIterator(data))
				.minWords(minWordFreq).stopWords(StopWords.getStopWords())
				.tokenize(new DefaultTokenizerFactory()).build();
		vectorizer.fit();
		return vectorizer.vocab();
	}

	/**
	 * Create embeddings (projections) for the content (data)
	 * 
	 * @param data
	 *            Collection of posts (documents, ...)
	 * @param wordVectors
	 *            Loaded Word2Vec embedding vectors
	 * @return The embedding for the data passed as argument.
	 */
	public static INDArray project(Collection<String> data,
			WordVectors wordVectors) {
		VocabCache cache = getVocab(data);
		if (cache == null || CollectionUtils.isEmpty(cache.tokens())
				|| wordVectors == null)
			return null;
		List<VocabWord> list = new ArrayList<VocabWord>(cache.tokens());
		INDArray matrix = null;
		int i = 0;
		do {
			String word = list.get(i).getWord();
			matrix = wordVectors.getWordVectorMatrix(word);
			if (matrix != null) {
				i++;
				break;
			}
		} while (++i < list.size());
		int total = 1;
		for (; i < list.size(); i++) {
			String word = list.get(i).getWord();
			if (wordVectors.getWordVectorMatrix(word) == null)
				continue;
			INDArray matr = wordVectors.getWordVectorMatrix(word);
			if (matr == null)
				continue;
			int freq = cache.wordFrequency(word);
			total += freq;
			matrix.add(matr.mul(freq));
		}
		return matrix == null ? matrix : matrix.div(total);
	}

	/**
	 * Return the most likely class based on the class labels probability
	 * distributions
	 * 
	 * @param guess
	 *            class probabilities distribution
	 * @return a class label
	 */
	public static int getBestGuess(INDArray guess) {
		if (guess == null)
			throw new UnsupportedOperationException(
					"Inferrence is null. Cannot classify \"null\".");
		int guessMax = 0;
		double max = guess.getDouble(0);
		for (int col = 1; col < guess.columns(); col++) {
			if (guess.getDouble(col) > max) {
				max = guess.getDouble(col);
				guessMax = col;
			}
		}
		return guessMax;
	}
}
